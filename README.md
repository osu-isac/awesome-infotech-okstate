# Awesome Information Technology Resources [![Awesome](https://cdn.rawgit.com/sindresorhus/awesome/d7305f38d29fed78fa85652e3a63e154dd8e8829/media/badge.svg)](https://github.com/sindresorhus/awesome)

<a href="https://www.raspberrypi.org"><img src="https://gitlab.com/osu-isac/awesome-infotech-okstate/raw/master/isac%20logo%202%20colors-inside%20white.png" alt="ISAC Logo" align="left" style="margin-right: 25px" height=150></a>

> Oklahoma State Uniersity Information Security and Assurance Club

This list is a collection of tools, projects, images and resources conforming to the [Awesome Manifesto](https://github.com/sindresorhus/awesome/blob/master/awesome.md).

Contributions *very welcome* but first see [Contributing](#contributing)

## Contents

- [Hacking](#hacking)
- [Computer Science](#comp-sci)
- [Networking](#networking)
- [Operating Systems](#operating-systems)
- [Professional Information Technology](#career)
- [Scripting](#scripting)
- [Development Operations](#devops)
- [_____ (blank) as a Service](#baas)
- [Programming Frameworks](#prog-frameworks)
- [Software Engineering](#sw-engineering)
- [Blockchain](#blockchain)
- [Homelab](#homelab)

## Hacking

## Computer Science
- [Open Source Computer Science Degree](https://github.com/ossu/computer-science)

## Networking

## Operating Systems

## Professional Information Technology

## Scripting

## Development Operations (DevOps)

### Source Control
* [What is git?](https://www.atlassian.com/git/tutorials/what-is-git) (brief primer on git source control)
* [GitLab](https://gitlab.com) (this is gitlab)
* [GitHub](https://github.com)

### Continuous Integration and Deployment (CI/CD)
* [Atlassian - What is CI/CD](https://www.atlassian.com/continuous-delivery/ci-vs-ci-vs-cd)
* [Awesome CI/CD](https://github.com/ciandcd/awesome-ciandcd) (yay for recursion!)

## _____ (blank) as a Service (BaaS)
* [Free for Dev - List of x as a service resources](https://github.com/ripienaar/free-for-dev) (yay recursion!)
* [_____ as a Service](https://en.wikipedia.org/wiki/As_a_service)
* [Heroku](https://www.heroku.com/) (Platform as a Service)

## Programming Frameworks
### Full Stack Frameworks
* [LAMP - Linux, Apache, MySQL, PHP](http://mern.io/) 
* [MERN - MongoDB, Express, React, Node](http://mern.io/)
* [Meteor](https://www.meteor.com/developers)
* [Ruby on Rails](http://rubyonrails.org/) (I believe this is what GitLab uses, ironically)

### Individual Language Frameworks
#### PHP
* [Laravel](https://github.com/laravel/laravel)

#### JavaScript
##### Front End
* [React](https://reactjs.org/)
* [Vue](https://vuejs.org/)
* [Angular](https://angularjs.org/)

#### Backend
* [Express](https://expressjs.com/)


## Software Engineering

## Blockchain
Resources for learning, working with, and understanding blockchain/distributed ledger systems.
* [Awesome Blockchains](https://github.com/openblockchains/awesome-blockchains) (awesome list in side awesome list) 

## Homelab


### License
[![CC0](http://mirrors.creativecommons.org/presskit/buttons/88x31/svg/cc-zero.svg)](https://creativecommons.org/publicdomain/zero/1.0/)[![Awesome](https://awesome.re/badge.svg)](https://awesome.re)